#ifndef SCENE_H
#define SCENE_H

#include <SFML/Graphics.hpp>


class Scene {
public: 
    Scene(sf::RenderWindow& window) : window(window) {}

    virtual ~Scene() {}

    virtual void init() = 0;
    virtual void update(float deltaTime) = 0;
    virtual void render(float deltaTime) = 0;
protected: 
    sf::RenderWindow& window;
    
};

sf::Font font;
Scene* currentScene;
Scene* nextScene = nullptr;


#endif