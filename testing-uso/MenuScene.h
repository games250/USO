#ifndef MENUSCENE_H
#define MENUSCENE_H

#include <SFML/Graphics.hpp>

#include "Scene.h"
#include "GameScene.h"


class MenuScene: public Scene {


public: 
    MenuScene(sf::RenderWindow& window) : Scene(window) {}

    void init() override {
        text.setFont(font);
        text.setString("Ukoncit hru:(");
        text.setCharacterSize(35);
        text.setFillColor(sf::Color::Black);

        text2.setFont(font);
        text2.setString("!USO");
        text2.setCharacterSize(100);
        text2.setFillColor(sf::Color::White);

        textStart.setFont(font);
        textStart.setString("StartUwu");
        textStart.setCharacterSize(85);
        textStart.setFillColor(sf::Color::Black);

        sf::Vector2f size = (sf::Vector2f)window.getSize();

    
        sf::Vector2f sizeOfRectangle = (sf::Vector2f)rectangle.getSize();
        rectangle.setFillColor(sf::Color::Red);
        rectangle.setPosition(sf::Vector2f((size.x / 2) - sizeOfRectangle.x / 2, (size.y / 2) + size.y / 3));

        
        sf::Vector2f sizeOfRectangleStart = (sf::Vector2f)rectangleStart.getSize();
        rectangleStart.setFillColor(sf::Color::Green);
        rectangleStart.setPosition(sf::Vector2f((size.x / 2) - sizeOfRectangleStart.x / 2, (size.y / 2) + size.y / 5));
        

        text.setPosition(rectangle.getPosition());
        text2.setPosition((size.x / 2) - text2.getLocalBounds().width / 2,  (size.y / 2) - size.y / 3);
        textStart.setPosition(rectangleStart.getPosition());

        rect.left = rectangle.getPosition().x;
        rect.top = rectangle.getPosition().y;
        rect.height = rectangle.getSize().y;
        rect.width = rectangle.getSize().x;

        rectStart.left = rectangleStart.getPosition().x;
        rectStart.top = rectangleStart.getPosition().y;
        rectStart.height = rectangleStart.getSize().y;
        rectStart.width = rectangleStart.getSize().x;
    }

    void update(float deltaTime) override {
       
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)){
            
            sf::Vector2i position = sf::Mouse::getPosition(window);
            
            if( rect.contains((sf::Vector2f)position) ){
                printf("+-----------------------+\n");
                printf("| Thank you for playing |\n");
                printf("+-----------------------+\n");


                
                window.close();
            }

            if( rectStart.contains((sf::Vector2f)position) ){

                nextScene = new GameScene(window);     
            }
        }
    }

    void render(float deltaTime) override {
        
        window.clear();

        window.draw(text2);
        window.draw(rectangle);
        window.draw(rectangleStart);
        window.draw(text);
        window.draw(textStart);
        
        window.display();
    }

private:
    sf::Rect<float> rect;
    sf::Rect<float> rectStart;

    sf::Text text;
    sf::Text text2;
    sf::Text textStart;
    sf::RectangleShape rectangle{sf::Vector2f(200.f, 50.f)};
    sf::RectangleShape rectangleStart{sf::Vector2f(350.f, 100.f)};
};

#endif