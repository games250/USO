#ifndef GAMESCENE_H
#define GAMESCENE_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Scene.h"

#include <chrono>
#include <thread>
#include <time.h>
#include <vector>
#include <math.h>

#define MAX_TIME_TO_KILL 3
#define SIZE_OF_RING 150
#define SIZE_OF_OUTERCIRCLE 50.0f
#define SIZE_OF_INNERCIRCLE 20.0f
#define SIZE_OF_MIDDLECIRCLE 35.0f
#define MAX_TIME_TO_KILL_CROSS 1
#define MAX_TIME_TO_KILL_TEXT 0.6f
#define MAX_AMMOUNT_OF_CIRCLES 4
#define MAX_AMMOUNT_OF_CROSSES 4

struct Cross{
    sf::RectangleShape line1;
    sf::RectangleShape line2;
    float timeToKill = MAX_TIME_TO_KILL_CROSS;
    bool isVisibale = false;

};


struct Hit{
    float timeToKill = MAX_TIME_TO_KILL_TEXT;
    sf::Text textHit;
    bool isVisible = false;
};

struct Circle{

    sf::CircleShape ring;
    float timeToKill = MAX_TIME_TO_KILL;
    sf::CircleShape outerCircle;
    sf::CircleShape middleCircle;
    sf::CircleShape innerCircle;
    

};

struct Miss{
    float timeToKill = MAX_TIME_TO_KILL_TEXT;
    sf::Text textMiss;
    bool isVisible = false;
};



class GameScene: public Scene {
public:

    GameScene(sf::RenderWindow& window) : Scene(window) {}
    
    void startCount(){
        using namespace std::chrono_literals;

        window.clear();
        window.draw(background);

        sf::Text startCountText;
        startCountText.setFont(font);
        startCountText.setString("3");
        startCountText.setCharacterSize(200);
        startCountText.setPosition((windowSize.x / 2) - startCountText.getLocalBounds().width / 2,  (windowSize.y / 2) - startCountText.getLocalBounds().height);
        startCountText.setFillColor(sf::Color::Red);
        window.draw(startCountText);
        window.display();

        //delay
        std::this_thread::sleep_for(1s);
        window.clear();
        window.draw(background);

        startCountText.setString("2");
        window.draw(startCountText);
        window.display();

        //delay
        std::this_thread::sleep_for(1s);
        window.clear();
        window.draw(background);
        startCountText.setString("1");
        window.draw(startCountText);
        window.display();

        //delay
        std::this_thread::sleep_for(1s);
        window.clear();
        window.draw(background);


    }

    void createScoreText(){
        scoreText.setFillColor(sf::Color::White);
        scoreText.setString("SCORE: 0"); 
        scoreText.setFont(font);
        scoreText.setCharacterSize(30);
        scoreText.setPosition((windowSize.x / 2) - scoreText.getLocalBounds().width / 2,  30 - scoreText.getLocalBounds().height);
    }
    

    void init() override {
        if (!music.openFromFile("Unravel.wav"))
        {
         // error...
         printf("error\n");
        } 
        music.setLoop(true);
        music.setVolume(2);
        music.play();
        
        if (!click.openFromFile("click.wav"))
        {
         // error...
         printf("error\n");
        } 
        if (!texture.loadFromFile("rem.jpg"))
        {
         // error...
         
        } 
        
        background.setTexture(texture);
        background.setColor(sf::Color(61, 61, 61));
       
        window.draw(background);
        
        startCount();

        createScoreText();

        for(int i = 0; i < MAX_AMMOUNT_OF_CIRCLES; i++){
            createCircle(circles[i]);
        }
    }


    Miss writeMiss(sf::Vector2i position){
        Miss miss;
        
        miss.textMiss.setString("MISS"); 
        miss.textMiss.setFillColor(sf::Color::Red);
        miss.textMiss.setFont(font);
        miss.textMiss.setCharacterSize(16);

        position.x -= miss.textMiss.getLocalBounds().width / 2;
        position.y -= (1.5 * (miss.textMiss.getLocalBounds().height));
        miss.textMiss.setPosition((sf::Vector2f)position);

        return miss;
    }

    Hit writeHit(Circle circle, std::string hitString){
        Hit hit;
        sf::Vector2f position;
        //hit.textHit.setString(whichHit); 
        //hit.textHit.setFillColor(sf::Color::Red);
        hit.textHit.setFont(font);
        hit.textHit.setCharacterSize(23);
        hit.textHit.setString(hitString);
        position.x = circle.outerCircle.getPosition().x - (hit.textHit.getLocalBounds().width / 2);
        position.y = circle.outerCircle.getPosition().y - (hit.textHit.getLocalBounds().height);
        
        hit.textHit.setPosition(position);

        return hit;
    }

    void createCross(Cross& cross, Circle circles){
        float width = 2 * SIZE_OF_OUTERCIRCLE;
        float height = 10;
        sf::Vector2f sizeOfCross(width, height);
        cross.line1.setSize(sizeOfCross);
        cross.line1.setPosition(circles.middleCircle.getPosition());
        cross.line1.setOrigin(width / 2, height / 2);
        cross.line1.setFillColor(sf::Color::Red);
        cross.line1.setRotation(45);

        cross.line2.setSize(sizeOfCross);
        cross.line2.setPosition(circles.middleCircle.getPosition());
        cross.line2.setOrigin(width / 2, height / 2);
        cross.line2.setFillColor(sf::Color::Red);
        cross.line2.setRotation(135);
        cross.timeToKill = MAX_TIME_TO_KILL_CROSS;

        cross.isVisibale = true;
    }

    void createCircle(Circle& circle){
        sf::Vector2f coord = getRandomCoor(); 
        //printf("posice x: %f \n",  coord.x);
        //printf("pozice y: %f \n",  coord.y); 

        circle.innerCircle.setOrigin(SIZE_OF_INNERCIRCLE, SIZE_OF_INNERCIRCLE);
        circle.innerCircle.setPosition(coord);
        circle.innerCircle.setFillColor(sf::Color(115, 242, 90, 200));
        circle.innerCircle.setRadius(SIZE_OF_INNERCIRCLE);
        circle.timeToKill = MAX_TIME_TO_KILL;
        
        circle.middleCircle.setOrigin(SIZE_OF_MIDDLECIRCLE, SIZE_OF_MIDDLECIRCLE);
        circle.middleCircle.setPosition(coord);
        circle.middleCircle.setFillColor(sf::Color(32, 189, 58, 200));
        circle.middleCircle.setRadius(SIZE_OF_MIDDLECIRCLE);

        circle.outerCircle.setOrigin(SIZE_OF_OUTERCIRCLE, SIZE_OF_OUTERCIRCLE);
        circle.outerCircle.setPosition(coord);
        circle.outerCircle.setFillColor(sf::Color(0, 138, 62, 200));
        circle.outerCircle.setRadius(SIZE_OF_OUTERCIRCLE);

        circle.ring.setOrigin(SIZE_OF_RING, SIZE_OF_RING);
        circle.ring.setPosition(coord);
        circle.ring.setFillColor(sf::Color(0,0,0,0));
        circle.ring.setOutlineColor(sf::Color(255, 255, 255, 200));
        circle.ring.setOutlineThickness(3);
        circle.ring.setRadius(SIZE_OF_RING);

    }

    sf::Vector2f getRandomCoor(){
        //sirka = min resolution(0) + 50, max resolution(u me 1920) - 50
        //vyska = min resolution(0) + 50, max resolution(u me 1080) - 50
        //printf("Random value on [0,%d]: %d\n", size.x , random_variable);
 
        // roll a 6-sided die 20 times
        
        int x = SIZE_OF_OUTERCIRCLE + SIZE_OF_RING + rand()/((RAND_MAX + 1u)/(windowSize.x - 2*(SIZE_OF_OUTERCIRCLE + SIZE_OF_RING))); // Note: 1+rand()%6 is biased
        //printf("%d \n",  x); 

        
        int y = 80 + rand()/((RAND_MAX + 1u)/(windowSize.y - 130)); // Note: 1+rand()%6 is biased
        //printf("%d \n",  y);
        sf::Vector2f coor{(float)x,(float)y};
        return coor;
        
    }


    

    void update(float deltaTime) override {
        //printf("cas: %f\n", deltaTime);
        
        if(skipframe == true){
            skipframe = false;
            return ;

        }
        for(size_t i = 0; i < hitTexts.size(); i++){
            hitTexts[i].timeToKill -= deltaTime;
        }
        for(size_t i = 0; i < missTexts.size(); i++){
            missTexts[i].timeToKill -= deltaTime;
        }
        for(int i = 0; i < MAX_AMMOUNT_OF_CIRCLES; i++){
            circles[i].timeToKill -= deltaTime;
            crosses[i].timeToKill -= deltaTime;
        }
        
        std::vector<int> dead;
        sf::Vector2i positionOfMouse;
        bool isHit = (sf::Mouse::isButtonPressed(sf::Mouse::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Q) || sf::Keyboard::isKeyPressed(sf::Keyboard::W));
        
        if (isHit && !wasHit){
            
            positionOfMouse = sf::Mouse::getPosition(window);
            
            for(int i = 0; i < MAX_AMMOUNT_OF_CIRCLES; i++){
                sf::Vector2f diff = circles[i].outerCircle.getPosition() - (sf::Vector2f)positionOfMouse;
                if ((sqrt((diff.x * diff.x) + (diff.y * diff.y))) <= SIZE_OF_OUTERCIRCLE){
                    click.openFromFile("click.wav");
                    click.setVolume(100);
                    click.play();
                }
                if ((sqrt((diff.x * diff.x) + (diff.y * diff.y))) <= SIZE_OF_INNERCIRCLE){
                    dead.push_back(i);
                    score += 100;
                    hitTexts.push_back(writeHit(circles[i], "Perfect!"));
                    hitTexts[hitTexts.size() - 1].isVisible = true;
                    
                    hitTexts[hitTexts.size() - 1].textHit.setFillColor(sf::Color(115, 242, 90, 200));
                } else if((sqrt((diff.x * diff.x) + (diff.y * diff.y))) <= SIZE_OF_MIDDLECIRCLE){
                    dead.push_back(i);
                    score += 90;
                    hitTexts.push_back(writeHit(circles[i], "Great!"));
                    hitTexts[hitTexts.size() - 1].isVisible = true;
                    
                    hitTexts[hitTexts.size() - 1].textHit.setFillColor(sf::Color(32, 189, 58, 200));
                } else if ((sqrt((diff.x * diff.x) + (diff.y * diff.y))) <= SIZE_OF_OUTERCIRCLE){
                    dead.push_back(i);
                    score += 80;
                    hitTexts.push_back(writeHit(circles[i], "Good!"));
                    hitTexts[hitTexts.size() - 1].isVisible = true;
                   
                    hitTexts[hitTexts.size() - 1].textHit.setFillColor(sf::Color(0, 138, 62, 200));
                }
                
            }
            

           
            if(dead.size() > 0){
                int indexOfNewestCircle = dead[0];
                
                for(size_t i = 1; i < dead.size(); i++){
                
                    if(circles[dead[i]].timeToKill > circles[indexOfNewestCircle].timeToKill){
                        indexOfNewestCircle = dead[i];
                    }
                }
                createCircle(circles[indexOfNewestCircle]);

            } else {
                miss.openFromFile("miss.wav");
                miss.setVolume(30);
                miss.play();
                score -= 50;
                missTexts.push_back(writeMiss(positionOfMouse));
                missTexts[missTexts.size() - 1].isVisible = true;
            }
        } 

        for(int i = 0; i < MAX_AMMOUNT_OF_CIRCLES; i++){
            if ( circles[i].timeToKill <= 0){
                missNoClick.openFromFile("missnoclick.wav");
                missNoClick.setVolume(100);
                missNoClick.play();
                createCross(crosses[i], circles[i]);
                //crosses[i].isVisibale = true;
                //odecist body a vygenerovat novy
                createCircle(circles[i]);
                score -= 100;
            }else{
                
                float scale = (SIZE_OF_OUTERCIRCLE/SIZE_OF_RING + circles[i].timeToKill/MAX_TIME_TO_KILL * (1 - SIZE_OF_OUTERCIRCLE/SIZE_OF_RING));
                circles[i].ring.setScale(scale,scale);
            }

            if(crosses[i].timeToKill <= 0){
                crosses[i].isVisibale = false;
            }
        }

        for(size_t i = 0; i < hitTexts.size(); i++){
            if(hitTexts[i].timeToKill <= 0){
                hitTexts[i].isVisible = false;
            } 
        }

        for(size_t i = 0; i < missTexts.size(); i++){
            if(missTexts[i].timeToKill <= 0){
                missTexts[i].isVisible = false;
            } 
        }
        
        std::string tmpScore1 = "Score: ";
        std::string tmpScore2 = std::to_string(score);

        std::string scoreTextik = tmpScore1 + tmpScore2;
        scoreText.setString(scoreTextik);
        wasHit = isHit;
    }

    void render(float deltaTime) override {

        int countOfMisstexts = missTexts.size();
        int countOfHittexts = hitTexts.size();
        window.clear();
        window.draw(background);
        window.draw(scoreText);

        for(int i = 0; i < MAX_AMMOUNT_OF_CIRCLES; i++){
            if(crosses[i].isVisibale){
                window.draw(crosses[i].line1);
                window.draw(crosses[i].line2);
            }


            window.draw(circles[i].ring);
            window.draw(circles[i].outerCircle);
            window.draw(circles[i].middleCircle);
            window.draw(circles[i].innerCircle);

            
        }

        for(int i = 0; i < countOfMisstexts; i++){
            if(missTexts[i].isVisible){
                window.draw(missTexts[i].textMiss);
            }
            
        }

        for(int i = 0; i < countOfHittexts; i++){
            if(hitTexts[i].isVisible){
                window.draw(hitTexts[i].textHit);
            }
            
        }
       
        window.display();
        
    }
private:
    sf::Vector2f windowSize = (sf::Vector2f)window.getSize();
    std::vector<Circle> circles{MAX_AMMOUNT_OF_CIRCLES};
    std::vector<Cross> crosses{MAX_AMMOUNT_OF_CROSSES};
    std::vector<Miss> missTexts;
    std::vector<Hit> hitTexts;
    bool wasHit = false;
    bool skipframe = true;
    sf::Text scoreText;
    int score = 0;
    sf::Sprite background;
    sf::Texture texture;
    sf::Music music;
    sf::Music click;
    sf::Music miss;
    sf::Music missNoClick;
};
#endif