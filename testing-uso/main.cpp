#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <filesystem>
#include <iostream>

#include <stdio.h>
#include "Scene.h"
#include "GameScene.h"
#include "MenuScene.h"


int main(int argc, char** argv) {
    sf::Clock clock;

    srand(time(NULL)); // use current time as seed for random generator
    current_path(std::filesystem::path(argv[0]).parent_path());
    font.loadFromFile("arial.ttf");
    
    
    sf::RenderWindow window(sf::VideoMode(1920, 1080), "USO!-MENU!", sf::Style::Fullscreen);//, sf::Style::Fullscreen
    currentScene = new MenuScene(window);

    currentScene->init();
    //std::cout << std::filesystem::path(argv[0]).parent_path() << "\n";
    
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
            
            if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape) {
                window.close();
            }

        }
        float deltaTime = clock.restart().asSeconds();

        currentScene->update(deltaTime);
        currentScene->render(deltaTime);

        if (nextScene != nullptr) {
            delete currentScene;
            currentScene = nextScene;
            nextScene = nullptr;
            currentScene->init();
        }
    }
    
    return 0;
}